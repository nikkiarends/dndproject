<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCharactersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('characters', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('name');
            $table->integer('level');
            $table->unsignedBigInteger('race_id');
            $table->foreign('race_id')->references('id')->on('races')->onDelete('cascade');
            $table->unsignedBigInteger('class_id');
            $table->foreign('class_id')->references('id')->on('classes')->onDelete('cascade');
            $table->integer('max_hitpoints');
            $table->integer('current_hitpoints');
            $table->integer('temporary_hitpoints');
            $table->unsignedBigInteger('alignment_id');
            $table->foreign('alignment_id')->references('id')->on('alignments')->onDelete('cascade');
            $table->unsignedBigInteger('background_id');
            $table->foreign('background_id')->references('id')->on('backgrounds')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('characters');
    }
}

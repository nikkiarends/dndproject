import Vuex from 'vuex'
import Vue from 'vue'
import axios from './http'
import router from './router'

Vue.use(Vuex)

export default new Vuex.Store({})

import Vue from 'vue'
import Router from 'vue-router'
import CharactersOverview from './components/CharactersOverview.vue'
import PlayerHandbook from './components/PlayerHandbook.vue'
import Home from './components/Home.vue'
import UserOverview from './components/UserOverview.vue'
import CharacterSheet from './components/CharacterSheet.vue'

Vue.use(Router)

const routes = [

    {
        path: '/characters-overview',
        name: 'characters',
        component: CharactersOverview,
    },

    {
        path: '/player-handbook',
        name: 'handbook',
        component: PlayerHandbook,
    },

    {
        path: '/',
        name: 'home',
        component: Home,
    },

    {
        path: '/userbase',
        name: 'userbase',
        component: UserOverview,
    },

    {
        path: '/character/:id',
        name: 'character.view',
        component: CharacterSheet,

    },

]


export default new Router({
    routes,
});

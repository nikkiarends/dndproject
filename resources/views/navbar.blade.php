<style>
.title {
    color: #fce4ec;
    font-size: 25px;
    font-family: "New Rocker";
    text-indent: 50px;
}

.title:hover {
    text-decoration: none;
    color: #f48fb1;
}

.navigation {
    background-color: #bdbdbd;
    font-family: "Work Sans";
    height: 40px;

}
</style>

<link href="https://fonts.googleapis.com/css?family=New+Rocker&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Work+Sans&display=swap" rel="stylesheet">

<nav class="navbar navbar-dark bg-dark">
  <a class="title" href="/">
  <i style="color:PaleVioletRed" class="fas fa-dice-d20"></i>
  Nikcat's character database
  </a>  
</nav>

<nav class="navbar navbar-expand-lg navbar-light navigation" >
    <ul style="text-indent: 90px;" class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="/my-characters">My characters<span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Player handbook</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Userbase</a>
      </li>
    </ul>
</nav>
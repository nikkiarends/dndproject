<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alignment extends Model
{
    public function characters()
    {
        return $this->belongsToMany('Character');

    }
}

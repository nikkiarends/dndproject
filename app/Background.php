<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Background extends Model
{
    public function characters()
    {
        return $this->belongsToMany('Character');

    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use App\Race;


class Character extends Model
{
    public function race()
    {
        return $this->hasOne('Race');
    }

    public function class() {
        return $this->hasMany('CharacterClass');
    }

    public function alignment()
    {
        return $this->hasOne('Alignment');
    }

    public function background()
    {
        return $this->hasOne('Background');
    }

    public function user()
    {
        return $this->belongsTo('User');
    }

}

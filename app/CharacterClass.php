<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CharacterClass extends Model
{
    public function characters()
    {
        return $this->belongsToMany('Character');

    }
}

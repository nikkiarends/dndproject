<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Character;

class Race extends Model
{
    public function characters()
    {
        return $this->belongsToMany('Character');

    }
}
